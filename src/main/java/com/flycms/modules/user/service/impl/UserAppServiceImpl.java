package com.flycms.modules.user.service.impl;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.UserApp;
import com.flycms.modules.user.mapper.UserAppMapper;
import com.flycms.modules.user.service.IUserAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.user.domain.dto.UserAppDto;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户用小程序注册信息Service业务层处理
 * 
 * @author admin
 * @date 2020-05-30
 */
@Service
public class UserAppServiceImpl implements IUserAppService
{
    private static final Logger log = LoggerFactory.getLogger(UserAppServiceImpl.class);

    @Autowired
    private UserAppMapper userAppMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户用小程序注册信息
     *
     * @param userApp 用户用小程序注册信息
     * @return 结果
     */
    @Override
    public int insertUserApp(UserApp userApp)
    {
        userApp.setId(SnowFlakeUtils.nextId());
        userApp.setCreateTime(DateUtils.getNowDate());
        return userAppMapper.insertUserApp(userApp);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除用户用小程序注册信息
     *
     * @param ids 需要删除的用户用小程序注册信息ID
     * @return 结果
     */
    @Override
    public int deleteUserAppByIds(Long[] ids)
    {
        return userAppMapper.deleteUserAppByIds(ids);
    }

    /**
     * 删除用户用小程序注册信息信息
     *
     * @param id 用户用小程序注册信息ID
     * @return 结果
     */
    @Override
    public int deleteUserAppById(Long id)
    {
        return userAppMapper.deleteUserAppById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户用小程序注册信息
     *
     * @param userApp 用户用小程序注册信息
     * @return 结果
     */
    @Override
    public int updateUserApp(UserApp userApp)
    {
        return userAppMapper.updateUserApp(userApp);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验openid是否唯一
     *
     * @param userApp 用户用小程序注册信息
     * @return 结果
     */
     @Override
     public String checkUserAppOpenIdUnique(UserApp userApp)
     {
         int count = userAppMapper.checkUserAppOpenIdUnique(userApp);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }

    /**
     * 校验unionid是否唯一
     *
     * @param userApp 用户用小程序注册信息
     * @return 结果
     */
     @Override
     public String checkUserAppUnionIdUnique(UserApp userApp)
     {
         int count = userAppMapper.checkUserAppUnionIdUnique(userApp);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }


    /**
     * 查询用户用小程序注册信息
     * 
     * @param id 用户用小程序注册信息ID
     * @return 用户用小程序注册信息
     */
    @Override
    public UserApp selectUserAppById(Long id)
    {
        return userAppMapper.selectUserAppById(id);
    }

    /**
     * 按openId查询用户用小程序注册信息详细信息
     *
     * @param openId 用户用小程序注册信息openId
     * @return 用户用小程序注册信息
     */
    @Override
    public UserApp selectUserAppByOpenId(String openId)
    {
        return userAppMapper.selectUserAppByOpenId(openId);
    }

    /**
     * 按unionId查询用户用小程序注册信息详细信息
     *
     * @param unionId 用户用小程序注册信息unionId
     * @return 用户用小程序注册信息
     */
    @Override
    public UserApp selectUserAppByUnionId(String unionId)
    {
        return userAppMapper.selectUserAppByUnionId(unionId);
    }

    /**
     * 查询用户用小程序注册信息列表
     *
     * @param userApp 用户用小程序注册信息
     * @return 用户用小程序注册信息
     */
    @Override
    public Pager<UserAppDto> selectUserAppPager(UserApp userApp, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<UserAppDto> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(userApp);

        List<UserApp> userAppList=userAppMapper.selectUserAppPager(pager);
        List<UserAppDto> dtolsit = new ArrayList<UserAppDto>();
        userAppList.forEach(userApps -> {
            UserAppDto userAppDto = new UserAppDto();
            userAppDto.setId(userApps.getId());
            userAppDto.setOpenId(userApps.getOpenId());
            userAppDto.setUnionId(userApps.getUnionId());
            userAppDto.setPlatform(userApps.getPlatform());
            userAppDto.setAvatarUrl(userApps.getAvatarUrl());
            userAppDto.setNickName(userApps.getNickName());
            userAppDto.setGender(userApps.getGender());
            userAppDto.setCountry(userApps.getCountry());
            userAppDto.setProvince(userApps.getProvince());
            userAppDto.setCity(userApps.getCity());
            userAppDto.setLanguage(userApps.getLanguage());
            dtolsit.add(userAppDto);
        });
        pager.setList(dtolsit);
        pager.setTotal(userAppMapper.queryUserAppTotal(pager));
        return pager;
    }

}
