package com.flycms.modules.user.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.UserAppMerge;
import com.flycms.modules.user.domain.dto.UserAppMergeDto;

/**
 * 用户和app关联Service接口
 * 
 * @author admin
 * @date 2020-05-31
 */
public interface IUserAppMergeService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户和app关联
     *
     * @param userAppMerge 用户和app关联
     * @return 结果
     */
    public int insertUserAppMerge(UserAppMerge userAppMerge);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除用户和app关联
     *
     * @param appIds 需要删除的用户和app关联ID
     * @return 结果
     */
    public int deleteUserAppMergeByIds(Long[] appIds);

    /**
     * 删除用户和app关联信息
     *
     * @param appId 用户和app关联ID
     * @return 结果
     */
    public int deleteUserAppMergeById(Long appId);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户和app关联
     *
     * @param userAppMerge 用户和app关联
     * @return 结果
     */
    public int updateUserAppMerge(UserAppMerge userAppMerge);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验APPID或者用户id是否唯一
     *
     * @param userAppMerge 用户和app关联
     * @return 结果
     */
    public String checkUserAppMergeUnique(UserAppMerge userAppMerge);


    /**
     * 查询用户和app关联
     * 
     * @param appId 用户和app关联ID
     * @return 用户和app关联
     */
    public UserAppMerge selectUserAppMergeById(Long appId);

    /**
     * 查询用户和app关联列表
     * 
     * @param userAppMerge 用户和app关联
     * @return 用户和app关联集合
     */
    public Pager<UserAppMergeDto> selectUserAppMergePager(UserAppMerge userAppMerge, Integer page, Integer limit, String sort, String order);
}
