package com.flycms.modules.user.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户分析查询关联表数据传输对象 fly_user_fans
 * 
 * @author admin
 * @date 2020-11-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserFansDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 被关注者id */
    @Excel(name = "被关注者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long followId;
    /** 关注者id */
    @Excel(name = "关注者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long fansId;

}
