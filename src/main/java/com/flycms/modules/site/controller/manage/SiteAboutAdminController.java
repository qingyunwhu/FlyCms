package com.flycms.modules.site.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.site.domain.SiteAbout;
import com.flycms.modules.site.domain.dto.SiteAboutDTO;
import com.flycms.modules.site.service.ISiteAboutService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 网站相关介绍Controller
 * 
 * @author admin
 * @date 2021-01-27
 */
@RestController
@RequestMapping("/system/site/about")
public class SiteAboutAdminController extends BaseController
{
    @Autowired
    private ISiteAboutService siteAboutService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增网站相关介绍
     */
    @PreAuthorize("@ss.hasPermi('site:about:add')")
    @Log(title = "网站相关介绍", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SiteAbout siteAbout)
    {
        return toAjax(siteAboutService.insertSiteAbout(siteAbout));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除网站相关介绍
     */
    @PreAuthorize("@ss.hasPermi('site:about:remove')")
    @Log(title = "网站相关介绍", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(siteAboutService.deleteSiteAboutByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改网站相关介绍
     */
    @PreAuthorize("@ss.hasPermi('site:about:edit')")
    @Log(title = "网站相关介绍", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SiteAbout siteAbout)
    {
        return toAjax(siteAboutService.updateSiteAbout(siteAbout));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询网站相关介绍列表
     */
    @PreAuthorize("@ss.hasPermi('site:about:list')")
    @GetMapping("/list")
    public TableDataInfo list(SiteAbout siteAbout,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SiteAboutDTO> pager = siteAboutService.selectSiteAboutPager(siteAbout, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出网站相关介绍列表
     */
    @PreAuthorize("@ss.hasPermi('site:about:export')")
    @Log(title = "网站相关介绍", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SiteAbout siteAbout)
    {
        List<SiteAboutDTO> siteAboutList = siteAboutService.exportSiteAboutList(siteAbout);
        ExcelUtil<SiteAboutDTO> util = new ExcelUtil<SiteAboutDTO>(SiteAboutDTO.class);
        return util.exportExcel(siteAboutList, "about");
    }

    /**
     * 获取网站相关介绍详细信息
     */
    @PreAuthorize("@ss.hasPermi('site:about:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(siteAboutService.findSiteAboutById(id));
    }

}
