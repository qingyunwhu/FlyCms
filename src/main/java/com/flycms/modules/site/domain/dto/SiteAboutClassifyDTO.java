package com.flycms.modules.site.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 关于我们分类数据传输对象 fly_site_about_classify
 * 
 * @author admin
 * @date 2021-01-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SiteAboutClassifyDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 分类ID */
    @Excel(name = "分类ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 分类名字 */
    @Excel(name = "分类名字")
    private String classifyName;
    /** 上级分类ID */
    @Excel(name = "上级分类ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

}
