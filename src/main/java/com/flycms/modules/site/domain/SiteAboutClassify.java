package com.flycms.modules.site.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 关于我们分类对象 fly_site_about_classify
 * 
 * @author admin
 * @date 2021-01-27
 */
@Data
public class SiteAboutClassify extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分类ID */
    private Long id;
    /** 分类名字 */
    private String classifyName;
    /** 上级分类ID */
    private Long parentId;
}
