package com.flycms.modules.score.controller.manage;


import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.score.domain.ScoreDetail;
import com.flycms.modules.score.domain.dto.ScoreDetailDTO;
import com.flycms.modules.score.service.IScoreDetailService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 积分日志Controller
 * 
 * @author admin
 * @date 2020-12-01
 */
@RestController
@RequestMapping("/system/score/scoreDetail")
public class ScoreDetailAdminController extends BaseController
{
    @Autowired
    private IScoreDetailService scoreDetailService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////


    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除积分日志
     */
    @PreAuthorize("@ss.hasPermi('score:scoreDetail:remove')")
    @Log(title = "积分日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(scoreDetailService.deleteScoreDetailByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询积分日志列表
     */
    @PreAuthorize("@ss.hasPermi('score:scoreDetail:list')")
    @GetMapping("/list")
    public TableDataInfo list(ScoreDetail scoreDetail,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<ScoreDetailDTO> pager = scoreDetailService.selectScoreDetailPager(scoreDetail, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出积分日志列表
     */
    @PreAuthorize("@ss.hasPermi('score:scoreDetail:export')")
    @Log(title = "积分日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ScoreDetail scoreDetail)
    {
        List<ScoreDetailDTO> scoreDetailList = scoreDetailService.exportScoreDetailList(scoreDetail);
        ExcelUtil<ScoreDetailDTO> util = new ExcelUtil<ScoreDetailDTO>(ScoreDetailDTO.class);
        return util.exportExcel(scoreDetailList, "scoreDetail");
    }

    /**
     * 获取积分日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('score:scoreDetail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(scoreDetailService.findScoreDetailById(id));
    }

}
