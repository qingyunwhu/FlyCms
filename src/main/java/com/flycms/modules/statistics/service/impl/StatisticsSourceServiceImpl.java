package com.flycms.modules.statistics.service.impl;


import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.statistics.mapper.StatisticsSourceMapper;
import com.flycms.modules.statistics.domain.StatisticsSource;
import com.flycms.modules.statistics.domain.dto.StatisticsSourceDTO;
import com.flycms.modules.statistics.service.IStatisticsSourceService;

import java.util.ArrayList;
import java.util.List;

/**
 * 来源统计Service业务层处理
 * 
 * @author admin
 * @date 2020-12-09
 */
@Service
public class StatisticsSourceServiceImpl implements IStatisticsSourceService 
{
    @Autowired
    private StatisticsSourceMapper statisticsSourceMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增来源统计
     *
     * @param statisticsSource 来源统计
     * @return 结果
     */
    @Override
    public int insertStatisticsSource(StatisticsSource statisticsSource)
    {
        statisticsSource.setId(SnowFlakeUtils.nextId());
        return statisticsSourceMapper.insertStatisticsSource(statisticsSource);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除来源统计
     *
     * @param ids 需要删除的来源统计ID
     * @return 结果
     */
    @Override
    public int deleteStatisticsSourceByIds(Long[] ids)
    {
        return statisticsSourceMapper.deleteStatisticsSourceByIds(ids);
    }

    /**
     * 删除来源统计信息
     *
     * @param id 来源统计ID
     * @return 结果
     */
    @Override
    public int deleteStatisticsSourceById(Long id)
    {
        return statisticsSourceMapper.deleteStatisticsSourceById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改来源统计
     *
     * @param statisticsSource 来源统计
     * @return 结果
     */
    @Override
    public int updateStatisticsSource(StatisticsSource statisticsSource)
    {
        return statisticsSourceMapper.updateStatisticsSource(statisticsSource);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询来源统计
     * 
     * @param id 来源统计ID
     * @return 来源统计
     */
    @Override
    public StatisticsSourceDTO findStatisticsSourceById(Long id)
    {
        StatisticsSource statisticsSource=statisticsSourceMapper.findStatisticsSourceById(id);
        return BeanConvertor.convertBean(statisticsSource,StatisticsSourceDTO.class);
    }


    /**
     * 查询来源统计列表
     *
     * @param statisticsSource 来源统计
     * @return 来源统计
     */
    @Override
    public Pager<StatisticsSourceDTO> selectStatisticsSourcePager(StatisticsSource statisticsSource, Integer page, Integer limit, String sort, String order)
    {
        Pager<StatisticsSourceDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(statisticsSource);

        List<StatisticsSource> statisticsSourceList=statisticsSourceMapper.selectStatisticsSourcePager(pager);
        List<StatisticsSourceDTO> dtolsit = new ArrayList<StatisticsSourceDTO>();
        statisticsSourceList.forEach(entity -> {
            StatisticsSourceDTO dto = new StatisticsSourceDTO();
            dto.setId(entity.getId());
            dto.setStatisticsDay(entity.getStatisticsDay());
            dto.setSorceUrlType(entity.getSorceUrlType());
            dto.setIsNewVisitor(entity.getIsNewVisitor());
            dto.setVisitorDeviceType(entity.getVisitorDeviceType());
            dto.setSourceDomain(entity.getSourceDomain());
            dto.setSorceUrl(entity.getSorceUrl());
            dto.setEngineName(entity.getEngineName());
            dto.setPvs(entity.getPvs());
            dto.setUvs(entity.getUvs());
            dto.setIps(entity.getIps());
            dto.setAccessHoureLong(entity.getAccessHoureLong());
            dto.setOnlyOnePv(entity.getOnlyOnePv());
            dto.setStatisticsHour(entity.getStatisticsHour());
            dto.setDeleted(entity.getDeleted());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(statisticsSourceMapper.queryStatisticsSourceTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的来源统计列表
     *
     * @param statisticsSource 来源统计
     * @return 来源统计集合
     */
    @Override
    public List<StatisticsSourceDTO> exportStatisticsSourceList(StatisticsSource statisticsSource) {
        return BeanConvertor.copyList(statisticsSourceMapper.exportStatisticsSourceList(statisticsSource),StatisticsSourceDTO.class);
    }
}
