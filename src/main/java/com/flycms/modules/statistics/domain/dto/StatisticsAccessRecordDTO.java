package com.flycms.modules.statistics.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 访问记录数据传输对象 fly_statistics_access_record
 * 
 * @author admin
 * @date 2020-12-09
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class StatisticsAccessRecordDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** id */
    @Excel(name = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 是否登录访问（0-否  1-是） */
    @Excel(name = "是否登录访问", readConverterExp = "0=-否,1=-是")
    private Boolean isLogin;
    /** 登录用户id */
    @Excel(name = "登录用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long loginUserId;
    /** 登录用户名 */
    @Excel(name = "登录用户名")
    private String loginUserName;
    /** 会话标识 */
    @Excel(name = "会话标识")
    private String sessionId;
    /** cookie标识 */
    @Excel(name = "cookie标识")
    private String cookieId;
    /** 访问来源(1:PC  2:移动端H5  3:微信客户端H5 4:IOS 5:安卓 6:小程序) */
    @Excel(name = "访问来源(1:PC  2:移动端H5  3:微信客户端H5 4:IOS 5:安卓 6:小程序)")
    private Short accessSourceClient;
    /** 访问网址 */
    @Excel(name = "访问网址")
    private String accessUrl;
    /** 来源网址 */
    @Excel(name = "来源网址")
    private String sourceUrl;
    /** 来源域名 */
    @Excel(name = "来源域名")
    private String sourceDomain;
    /** 来源网站类型 （1-搜索引擎  2-外部链接  3-直接访问） */
    @Excel(name = "来源网站类型 ", readConverterExp = "1=-搜索引擎,2=-外部链接,3=-直接访问")
    private Integer sorceUrlType;
    /** 访客ip */
    @Excel(name = "访客ip")
    private String accessIp;
    /** 访客设备系统（如：Win10 Mac10  Android8） */
    @Excel(name = "访客设备系统", readConverterExp = "如=：Win10,M=ac10,A=ndroid8")
    private String accessDevice;
    /** 访客所属城市 */
    @Excel(name = "访客所属城市")
    private String accessCity;
    /** 访客浏览器类型 */
    @Excel(name = "访客浏览器类型")
    private String accessBrowser;
    /** 访客所属国家 */
    @Excel(name = "访客所属国家")
    private String accessCountry;
    /** 访客所属省份 */
    @Excel(name = "访客所属省份")
    private String accessProvince;
    /** 搜索名称 */
    @Excel(name = "搜索名称")
    private String engineName;
    /** 是否新访客（0:否   1:是） */
    @Excel(name = "是否新访客", readConverterExp = "0=:否,1=:是")
    private Integer isNewVisitor;
    /** 设备类型 */
    @Excel(name = "设备类型")
    @JsonSerialize(using = ToStringSerializer.class)
    private Short deviceType;
    /** 删除标识 */
    @Excel(name = "删除标识")
    private Integer deleted;

}
