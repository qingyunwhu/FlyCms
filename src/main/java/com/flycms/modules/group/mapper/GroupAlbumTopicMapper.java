package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupAlbumTopic;
import org.springframework.stereotype.Repository;

/**
 * 小组专辑帖子关联Mapper接口
 * 
 * @author admin
 * @date 2020-12-16
 */
@Repository
public interface GroupAlbumTopicMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组专辑帖子关联
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 结果
     */
    public int insertGroupAlbumTopic(GroupAlbumTopic groupAlbumTopic);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组专辑帖子关联
     *
     * @param id 小组专辑帖子关联ID
     * @return 结果
     */
    public int deleteGroupAlbumTopicById(Long id);

    /**
     * 批量删除小组专辑帖子关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupAlbumTopicByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组专辑帖子关联
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 结果
     */
    public int updateGroupAlbumTopic(GroupAlbumTopic groupAlbumTopic);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询小组专辑帖子关联
     * 
     * @param id 小组专辑帖子关联ID
     * @return 小组专辑帖子关联
     */
    public GroupAlbumTopic findGroupAlbumTopicById(Long id);

    /**
     * 查询小组专辑帖子关联数量
     *
     * @param pager 分页处理类
     * @return 小组专辑帖子关联数量
     */
    public int queryGroupAlbumTopicTotal(Pager pager);

    /**
     * 查询小组专辑帖子关联列表
     * 
     * @param pager 分页处理类
     * @return 小组专辑帖子关联集合
     */
    public List<GroupAlbumTopic> selectGroupAlbumTopicPager(Pager pager);

    /**
     * 查询需要导出的小组专辑帖子关联列表
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 小组专辑帖子关联集合
     */
    public List<GroupAlbumTopic> exportGroupAlbumTopicList(GroupAlbumTopic groupAlbumTopic);
}
