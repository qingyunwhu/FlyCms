package com.flycms.modules.group.service.impl;

import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupUserIsaudit;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.modules.group.service.IGroupUserIsauditService;
import com.flycms.modules.user.service.IUserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.mapper.GroupUserMapper;
import com.flycms.modules.group.domain.GroupUser;
import com.flycms.modules.group.domain.dto.GroupUserDTO;
import com.flycms.modules.group.service.IGroupUserService;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 群组和用户对应关系Service业务层处理
 * 
 * @author admin
 * @date 2020-09-27
 */
@Service
public class GroupUserServiceImpl implements IGroupUserService 
{
    private static final Logger log = LoggerFactory.getLogger(GroupUserServiceImpl.class);

    @Autowired
    private GroupUserMapper groupUserMapper;
    @Autowired
    private IGroupService groupService;
    @Autowired
    private IGroupUserIsauditService groupUserIsauditService;
    @Autowired
    private IUserAccountService userAccountService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增群组和用户对应关系
     *
     * @param groupUser 群组和用户对应关系
     * @return 结果
     */
    @Override
    @Transactional
    public AjaxResult insertGroupUser(GroupUser groupUser){
        //查询群组信息
        Group group=groupService.findGroupById(groupUser.getGroupId());
        if(group != null){
            AjaxResult.error(501,"小组不存在");
        }

        //加入群组需要审核，isaudit 1审核，0不审核
        if("1".equals(group.getIsaudit())){
            GroupUserIsaudit isaudit=groupUserIsauditService.findGroupUserIsauditById(groupUser.getUserId(),groupUser.getGroupId());
            if(isaudit == null){
                GroupUserIsaudit groupUserIsaudit= new GroupUserIsaudit();
                groupUserIsaudit.setGroupId(groupUser.getGroupId());
                groupUserIsaudit.setUserId(groupUser.getUserId());
                groupUserIsauditService.insertGroupUserIsaudit(groupUserIsaudit);
                return AjaxResult.success(511,"已提交，请等待审核");
            }else{
                GroupUserIsaudit groupUserIsaudit= new GroupUserIsaudit();
                groupUserIsaudit.setGroupId(groupUser.getGroupId());
                groupUserIsaudit.setUserId(groupUser.getUserId());
                groupUserIsaudit.setStatus(0);
                groupUserIsauditService.updateGroupUserIsaudit(groupUserIsaudit);
                return AjaxResult.error(512,"已撤销申请加入");
            }
        }else{
            groupUser.setUserId(SessionUtils.getUser().getId());
            //查询管理用户和群组关联信息
            GroupUser findgroupUser = new GroupUser();
            findgroupUser.setGroupId(groupUser.getGroupId());
            findgroupUser.setUserId(groupUser.getUserId());
            GroupUser userDTO=groupUserMapper.findGroupUser(findgroupUser);
            if(userDTO != null){
                if(userDTO.getStatus() == 0){
                    groupUser.setStatus(1);
                    groupUserMapper.updateGroupUser(groupUser);
                    groupService.updateCountUser(group.getId());
                    userAccountService.updateUserGroup(groupUser.getUserId());
                    return AjaxResult.success(520,"已关注本群组");
                }else{
                    groupUser.setStatus(0);
                    groupUserMapper.updateGroupUser(groupUser);
                    groupService.updateCountUser(group.getId());
                    userAccountService.updateUserGroup(groupUser.getUserId());
                    return AjaxResult.error(521,"已退出小组");
                }
            }else{
                groupUser.setId(SnowFlakeUtils.nextId());
                groupUser.setCreateTime(DateUtils.getNowDate());
                groupUser.setStatus(1);
                groupUserMapper.insertGroupUser(groupUser);
                groupService.updateCountUser(group.getId());
                userAccountService.updateUserGroup(groupUser.getUserId());
                return AjaxResult.success(520,"已加入本群组");
            }
        }
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////

    /**
     * 删除群组和用户对应关系信息
     *
     * @param userId 群组和用户对应关系ID
     * @return 结果
     */
    @Override
    public int deleteGroupUserById(Long userId,Long groupId)
    {
        return groupUserMapper.deleteGroupUserById(userId,groupId);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改群组和用户对应关系
     *
     * @param groupUser 群组和用户对应关系
     * @return 结果
     */
    @Override
    public int updateGroupUser(GroupUser groupUser)
    {
        return groupUserMapper.updateGroupUser(groupUser);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 按用户ID和群组ID查询关系详细信息
     *
     * @param groupUser 群组和用户对应关系
     * @return 群组和用户对应关系
     */
    @Override
    public GroupUser findGroupUser(GroupUser groupUser)
    {
        return groupUserMapper.findGroupUser(groupUser);
    }


    /**
     * 查询群组和用户对应关系列表
     *
     * @param groupUser 群组和用户对应关系
     * @return 群组和用户对应关系
     */
    @Override
    public Pager<GroupUserDTO> selectGroupUserPager(GroupUser groupUser, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<GroupUserDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }else{
            pager.addOrderProperty("id", true,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupUser);

        List<GroupUser> groupUserList=groupUserMapper.selectGroupUserPager(pager);
        List<GroupUserDTO> dtolsit = new ArrayList<GroupUserDTO>();
        groupUserList.forEach(entity -> {
            GroupUserDTO dto = new GroupUserDTO();
            dto.setUserId(entity.getUserId());
            dto.setGroupId(entity.getGroupId());
            dto.setIsadmin(entity.getIsadmin());
            dto.setIsfounder(entity.getIsfounder());
            dto.setEndTime(entity.getEndTime());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupUserMapper.queryGroupUserTotal(pager));
        return pager;
    }

}
