package com.flycms.modules.group.controller.manage;

import java.util.List;
import com.flycms.common.constant.UserConstants;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupColumn;
import com.flycms.modules.group.domain.dto.GroupColumnDTO;
import com.flycms.modules.group.service.IGroupColumnService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;

/**
 * 小组分类Controller
 * 
 * @author admin
 * @date 2020-09-25
 */
@RestController
@RequestMapping("/system/group/column")
public class GroupColumnAdminController extends BaseController
{
    @Autowired
    private IGroupColumnService groupColumnService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增小组分类
     */
    @PreAuthorize("@ss.hasPermi('group:column:add')")
    @Log(title = "小组分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GroupColumn groupColumn)
    {
        if (UserConstants.NOT_UNIQUE.equals(groupColumnService.checkGroupColumnColumnNameUnique(groupColumn)))
        {
            return AjaxResult.error("新增小组分类'" + groupColumn.getColumnName() + "'失败，分类名称已存在");
        }
        return toAjax(groupColumnService.insertGroupColumn(groupColumn));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组分类
     */
    @PreAuthorize("@ss.hasPermi('group:column:remove')")
    @Log(title = "小组分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(groupColumnService.deleteGroupColumnByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组分类
     */
    @PreAuthorize("@ss.hasPermi('group:column:edit')")
    @Log(title = "小组分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupColumn groupColumn)
    {
        return toAjax(groupColumnService.updateGroupColumn(groupColumn));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询小组分类列表
     */
    @PreAuthorize("@ss.hasPermi('group:column:list')")
    @GetMapping("/list")
    public AjaxResult list(GroupColumn groupColumn)
    {
        List<GroupColumnDTO> list = groupColumnService.selectGroupColumnList(groupColumn);
        return AjaxResult.success(list);
    }

    /**
     * 导出小组分类列表
     */
    @PreAuthorize("@ss.hasPermi('group:column:export')")
    @Log(title = "小组分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GroupColumn groupColumn)
    {
        List<GroupColumnDTO> list = groupColumnService.selectGroupColumnList(groupColumn);
        ExcelUtil<GroupColumnDTO> util = new ExcelUtil<GroupColumnDTO>(GroupColumnDTO.class);
        return util.exportExcel(list, "column");
    }

    /**
     * 获取小组分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('group:column:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(groupColumnService.findGroupColumnById(id));
    }
}
