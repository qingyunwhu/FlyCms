package com.flycms.modules.group.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupUserIsaudit;
import com.flycms.modules.group.domain.dto.GroupUserIsauditDTO;
import com.flycms.modules.group.service.IGroupUserIsauditService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 小组成员审核Controller
 * 
 * @author admin
 * @date 2020-11-24
 */
@RestController
@RequestMapping("/system/group/isaudit")
public class GroupUserIsauditAdminController extends BaseController
{
    @Autowired
    private IGroupUserIsauditService groupUserIsauditService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增小组成员审核
     */
    @PreAuthorize("@ss.hasPermi('group:isaudit:add')")
    @Log(title = "小组成员审核", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GroupUserIsaudit groupUserIsaudit)
    {

        return toAjax(groupUserIsauditService.insertGroupUserIsaudit(groupUserIsaudit));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组成员审核
     */
    @PreAuthorize("@ss.hasPermi('group:isaudit:remove')")
    @Log(title = "小组成员审核", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(groupUserIsauditService.deleteGroupUserIsauditByIds(userIds));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组成员审核
     */
    @PreAuthorize("@ss.hasPermi('group:isaudit:edit')")
    @Log(title = "小组成员审核", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupUserIsaudit groupUserIsaudit)
    {

        return toAjax(groupUserIsauditService.updateGroupUserIsaudit(groupUserIsaudit));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询小组成员审核列表
     */
    @PreAuthorize("@ss.hasPermi('group:isaudit:list')")
    @GetMapping("/list")
    public TableDataInfo list(GroupUserIsaudit groupUserIsaudit,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupUserIsauditDTO> pager = groupUserIsauditService.selectGroupUserIsauditPager(groupUserIsaudit, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出小组成员审核列表
     */
    @PreAuthorize("@ss.hasPermi('group:isaudit:export')")
    @Log(title = "小组成员审核", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GroupUserIsaudit groupUserIsaudit)
    {
        List<GroupUserIsauditDTO> groupUserIsauditList = groupUserIsauditService.exportGroupUserIsauditList(groupUserIsaudit);
        ExcelUtil<GroupUserIsauditDTO> util = new ExcelUtil<GroupUserIsauditDTO>(GroupUserIsauditDTO.class);
        return util.exportExcel(groupUserIsauditList, "isaudit");
    }

    /**
     * 获取小组成员审核详细信息
     */
/*
    @PreAuthorize("@ss.hasPermi('group:isaudit:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return AjaxResult.success(groupUserIsauditService.findGroupUserIsauditById(userId));
    }
*/

}
