package com.flycms.modules.monitor.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.monitor.domain.dto.OperLogDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.monitor.domain.OperLog;
import com.flycms.modules.monitor.mapper.FlyOperLogMapper;
import com.flycms.modules.monitor.service.IFlyOperLogService;

/**
 * 操作日志 服务层处理
 * 
 * @author kaifei sun
 */
@Service
public class FlyOperLogServiceImpl implements IFlyOperLogService
{
    @Autowired
    private FlyOperLogMapper operLogMapper;

    /**
     * 新增操作日志
     * 
     * @param operLog 操作日志对象
     */
    @Override
    public void insertOperlog(OperLog operLog)
    {
        operLogMapper.insertOperlog(operLog);
    }


    /**
     * 批量删除系统操作日志
     * 
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    public int deleteOperLogByIds(Long[] operIds)
    {
        return operLogMapper.deleteOperLogByIds(operIds);
    }

    /**
     * 查询操作日志详细
     * 
     * @param operId 操作ID
     * @return 操作日志对象
     */
    @Override
    public OperLog selectOperLogById(Long operId)
    {
        return operLogMapper.selectOperLogById(operId);
    }

    /**
     * 清空操作日志
     */
    @Override
    public void cleanOperLog()
    {
        operLogMapper.cleanOperLog();
    }

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询操作日志记录列表
     *
     * @param operLog 操作日志记录
     * @return 操作日志记录
     */
    @Override
    public Pager<OperLogDTO> selectOperLogPager(OperLog operLog, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<OperLogDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }else{
            pager.addOrderProperty("id", true,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(operLog);

        List<OperLog> operLogList=operLogMapper.selectOperLogPager(pager);
        List<OperLogDTO> dtolsit = new ArrayList<OperLogDTO>();
        operLogList.forEach(entity -> {
            OperLogDTO dto = new OperLogDTO();
            dto.setId(entity.getId());
            dto.setTitle(entity.getTitle());
            dto.setBusinessType(entity.getBusinessType());
            dto.setMethod(entity.getMethod());
            dto.setRequestMethod(entity.getRequestMethod());
            dto.setOperatorType(entity.getOperatorType());
            dto.setOperName(entity.getOperName());
            dto.setDeptName(entity.getDeptName());
            dto.setOperUrl(entity.getOperUrl());
            dto.setOperIp(entity.getOperIp());
            dto.setOperLocation(entity.getOperLocation());
            dto.setOperParam(entity.getOperParam());
            dto.setJsonResult(entity.getJsonResult());
            dto.setStatus(entity.getStatus());
            dto.setErrorMsg(entity.getErrorMsg());
            dto.setOperTime(entity.getOperTime());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(operLogMapper.queryOperLogTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的操作日志记录列表
     *
     * @param operLog 操作日志记录
     * @return 操作日志记录集合
     */
    @Override
    public List<OperLogDTO> exportOperLogList(OperLog operLog) {
        return BeanConvertor.copyList(operLogMapper.exportOperLogList(operLog),OperLogDTO.class);
    }
}
