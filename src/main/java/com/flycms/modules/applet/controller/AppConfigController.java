package com.flycms.modules.applet.controller;

import com.flycms.modules.applet.domain.AppConfig;
import com.flycms.modules.applet.service.IAppConfigService;
import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import com.flycms.modules.applet.domain.dto.AppConfigDto;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 小程序开发者ID设置Controller
 * 
 * @author admin
 * @date 2020-05-27
 */
@RestController
@RequestMapping("/system/applet/appConfig")
public class AppConfigController extends BaseController
{
    @Autowired
    private IAppConfigService appConfigService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增小程序开发者ID设置
     */
    @PreAuthorize("@ss.hasPermi('applet:appConfig:add')")
    @Log(title = "开发者ID设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppConfig appConfig)
    {
        if (UserConstants.NOT_UNIQUE.equals(appConfigService.checkAppConfigUnique(appConfig)))
        {
            return AjaxResult.error("新增小程序开发者ID设置'" + appConfig.getAppId() + "'失败，AppId已存在");
        }
        return toAjax(appConfigService.insertAppConfig(appConfig));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小程序开发者ID设置
     */
    @PreAuthorize("@ss.hasPermi('applet:appConfig:remove')")
    @Log(title = "小程序开发者ID设置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(appConfigService.deleteAppConfigByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小程序开发者ID设置
     */
    @PreAuthorize("@ss.hasPermi('applet:appConfig:edit')")
    @Log(title = "小程序开发者ID设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppConfig appConfig)
    {
        return toAjax(appConfigService.updateAppConfig(appConfig));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询小程序开发者ID设置列表
     */
    @PreAuthorize("@ss.hasPermi('applet:appConfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppConfig appConfig,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<AppConfigDto> pager = appConfigService.selectAppConfigPager(appConfig, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出小程序开发者ID设置列表
     */
    @PreAuthorize("@ss.hasPermi('applet:appConfig:export')")
    @Log(title = "小程序开发者ID设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AppConfig appConfig,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<AppConfigDto> pager = appConfigService.selectAppConfigPager(appConfig, pageNum, pageSize, sort, order);
        ExcelUtil<AppConfigDto> util = new ExcelUtil<AppConfigDto>(AppConfigDto.class);
        return util.exportExcel(pager.getList(), "appConfig");
    }

    /**
     * 获取小程序开发者ID设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('applet:appConfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(appConfigService.selectAppConfigById(id));
    }
}
