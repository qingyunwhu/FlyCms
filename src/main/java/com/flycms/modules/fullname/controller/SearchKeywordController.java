package com.flycms.modules.fullname.controller;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.fullname.domain.SearchKeyword;
import com.flycms.modules.fullname.domain.dto.SearchKeywordDTO;
import com.flycms.modules.fullname.service.ISearchKeywordService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 姓名搜索Controller
 * 
 * @author admin
 * @date 2020-10-13
 */
@RestController
@RequestMapping("/system/fullname/keyword")
public class SearchKeywordController extends BaseController
{
    @Autowired
    private ISearchKeywordService searchKeywordService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增姓名搜索
     */
    @PreAuthorize("@ss.hasPermi('fullname:keyword:add')")
    @Log(title = "姓名搜索", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SearchKeyword searchKeyword)
    {
        return toAjax(searchKeywordService.insertSearchKeyword(searchKeyword));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除姓名搜索
     */
    @PreAuthorize("@ss.hasPermi('fullname:keyword:remove')")
    @Log(title = "姓名搜索", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(searchKeywordService.deleteSearchKeywordByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改姓名搜索
     */
    @PreAuthorize("@ss.hasPermi('fullname:keyword:edit')")
    @Log(title = "姓名搜索", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SearchKeyword searchKeyword)
    {
        return toAjax(searchKeywordService.updateSearchKeyword(searchKeyword));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询姓名搜索列表
     */
    @PreAuthorize("@ss.hasPermi('fullname:keyword:list')")
    @GetMapping("/list")
    public TableDataInfo list(SearchKeyword searchKeyword,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SearchKeywordDTO> pager = searchKeywordService.selectSearchKeywordPager(searchKeyword, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出姓名搜索列表
     */
    @PreAuthorize("@ss.hasPermi('fullname:keyword:export')")
    @Log(title = "姓名搜索", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SearchKeyword searchKeyword,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SearchKeywordDTO> pager = searchKeywordService.selectSearchKeywordPager(searchKeyword, pageNum, pageSize, sort, order);
        ExcelUtil<SearchKeywordDTO> util = new ExcelUtil<SearchKeywordDTO>(SearchKeywordDTO.class);
        return util.exportExcel(pager.getList(), "keyword");
    }

    /**
     * 获取姓名搜索详细信息
     */
    @PreAuthorize("@ss.hasPermi('fullname:keyword:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(searchKeywordService.findSearchKeywordById(id));
    }
}
