package com.flycms.modules.system.service;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.system.domain.FlyPost;
import com.flycms.modules.system.domain.dto.PostDTO;
import com.flycms.modules.system.domain.dto.PostQueryDTO;

/**
 * 岗位信息 服务层
 * 
 * @author kaifei sun
 */
public interface IFlyPostService
{
    /**
     * 查询所有岗位
     * 
     * @return 岗位列表
     */
    public List<PostQueryDTO> selectPostAll();

    /**
     * 通过岗位ID查询岗位信息
     * 
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    public FlyPost selectPostById(Long postId);

    /**
     * 按任务规则ID查询关联的岗位列表
     *
     * @param ruleId 任务规则ID
     * @return 角色对象信息
     */
    public List<FlyPost> selectTaskRulePostByRuleId(Long ruleId);

    /**
     * 根据用户ID获取岗位选择框列表
     * 
     * @param adminId 用户ID
     * @return 选中岗位ID列表
     */
    public List<Integer> selectPostListByAdminId(Long adminId);

    /**
     * 校验岗位名称
     * 
     * @param post 岗位信息
     * @return 结果
     */
    public String checkPostNameUnique(FlyPost post);

    /**
     * 校验岗位编码
     * 
     * @param post 岗位信息
     * @return 结果
     */
    public String checkPostCodeUnique(FlyPost post);

    /**
     * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    public int countAdminPostById(Long postId);

    /**
     * 删除岗位信息
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    public int deletePostById(Long postId);

    /**
     * 批量删除岗位信息
     * 
     * @param postIds 需要删除的岗位ID
     * @return 结果
     * @throws Exception 异常
     */
    public int deletePostByIds(Long[] postIds);

    /**
     * 新增保存岗位信息
     * 
     * @param post 岗位信息
     * @return 结果
     */
    public int insertPost(FlyPost post);

    /**
     * 修改保存岗位信息
     * 
     * @param post 岗位信息
     * @return 结果
     */
    public int updatePost(FlyPost post);


    /**
     * 查询岗位信息集合
     *
     * @param post 岗位信息
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    public Pager<PostDTO> selectPostPager(FlyPost post, Integer page, Integer limit, String sort, String order);
}
