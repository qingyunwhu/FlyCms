package com.flycms.modules.system.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 岗位表 fly_post
 * 
 * @author kaifei sun
 */
@Data
public class PostQueryDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 岗位序号 */
    private Long postId;

    /** 岗位名称 */
    private String postName;

    /** 状态（0正常 1停用） */
    private String status;


}
