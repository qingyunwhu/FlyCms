package com.flycms.modules.system.mapper;

import java.util.List;

import com.flycms.modules.system.domain.FlyAdmin;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.system.domain.dto.AdminQueryDTO;
import org.apache.ibatis.annotations.Param;

import org.springframework.stereotype.Repository;

/**
 * 用户表 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyAdminMapper {
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户信息
     *
     * @param admin 用户信息
     * @return 结果
     */
    public int insertAdmin(FlyAdmin admin);
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 通过用户ID删除用户
     *
     * @param adminId 用户ID
     * @return 结果
     */
    public int deleteAdminById(Long adminId);

    /**
     * 批量删除用户信息
     *
     * @param adminIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteAdminByIds(Long[] adminIds);
    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户信息
     *
     * @param admin 用户信息
     * @return 结果
     */
    public int updateAdmin(FlyAdmin admin);

    /**
     * 修改用户审核状态
     *
     * @param adminId
     * @param status
     * @return
     */
    public int updateAdminStatus(@Param("adminId") Long adminId,@Param("status") Integer status);

    /**
     * 修改用户头像
     *
     * @param adminName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public int updateAdminAvatar(@Param("adminName") String adminName, @Param("avatar") String avatar);

    /**
     * 重置用户密码
     *
     * @param adminName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetAdminPwd(@Param("adminName") String adminName, @Param("password") String password);
    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 根据条件查询用户数量
     *
     * @param pager 翻页类
     * @return 线索数量
     */
    public int queryAdminTotal(Pager pager);

    /**
     * 根据条件分页查询用户列表
     * 
     * @param pager 用户信息
     * @return 用户信息集合信息
     */
    public List<FlyAdmin> selectAdminPager(Pager pager);

    /**
     * 通过用户名查询用户
     * 
     * @param adminName 用户名
     * @return 用户对象信息
     */
    public FlyAdmin selectAdminByName(String adminName);

    /**
     * 通过用户ID查询用户
     * 
     * @param adminId 用户ID
     * @return 用户对象信息
     */
    public AdminQueryDTO selectAdminById(Long adminId);

    /**
     * 校验用户名称是否唯一
     * 
     * @param adminName 用户名称
     * @return 结果
     */
    public int checkAdminNameUnique(String adminName);

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return 结果
     */
    public FlyAdmin checkPhoneUnique(String phonenumber);

    /**
     * 校验email是否唯一
     *
     * @param email 用户邮箱
     * @return 结果
     */
    public FlyAdmin checkEmailUnique(String email);
}
