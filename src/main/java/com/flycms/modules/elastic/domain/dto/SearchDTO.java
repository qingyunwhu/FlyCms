package com.flycms.modules.elastic.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SearchDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /** ID */
    private String id;
    /** 信息类型，1话题，2用户，3标签 */
    private String infoType;
    /** 小组ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private String groupId;
    /** 分类ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private String columnId;
    /** 用户ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private String userId;
    /** 标题 */
    private String title;
    /** 内容 */
    private String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
