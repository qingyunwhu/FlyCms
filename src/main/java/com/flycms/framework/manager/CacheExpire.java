package com.flycms.framework.manager;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @author sun kaifei
 * 2021/2/26
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheExpire {
    /**
     * expire time, default 60s
     */
    @AliasFor("expire")
    long value() default 60L;

    /**
     * expire time, default 60s
     */
    @AliasFor("value")
    long expire() default 60L;

}
