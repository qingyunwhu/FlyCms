package com.flycms.common.utils;

import cn.hutool.dfa.WordTree;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 敏感词处理工具 - DFA算法实现
 * <p>
 * 源码来自：https://www.jianshu.com/p/2e84eacc3cc8
 *
 * @author sam
 * @since 2017/9/4
 * <p>
 * 这个过滤算法简单是简单，但没法对多音字过滤，一个敏感词，如果用同音的字把其中一个字换了，就过滤不掉了，不过聊胜于无 :)
 */
public class SensitiveWordUtils {



    /**
     * 替换敏感字字符
     *
     * @param txt        文本
     * @param replaceStr 替换的字符串，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符串：[屏蔽]，替换结果：我爱[屏蔽]
     * @return
     */
    public static String replaceSensitiveWord(String txt, List<String> replaceStr) {
        if(!StringUtils.isBlank(txt)){
            //过滤关键词去重
            List<String> replaceList = replaceStr.stream().distinct().collect(Collectors.toList());
            for (String str : replaceList) {
                if(str.length() > 0){
                    txt = filterSensitiveWords(txt, str);
                }
            };
        }
        return txt.trim();
    }


    /**
     * 过滤字符串中的敏感词汇
     * @param content   文本
     * @param sensitiveWord   敏感词汇
     * @return
     */
    public static  String filterSensitiveWords(String content, String sensitiveWord) {

        if (content == null || sensitiveWord == null) {
            return content;
        }

        //获取和敏感词汇相同数量的星号
        String starChar = getStarChar(sensitiveWord.length());

        //替换敏感词汇
        return content.replace(sensitiveWord, starChar);
    }

    //大部分敏感词汇在10个以内，直接返回缓存的字符串
    public static String[] starArr={"*","**","***","****","*****","******","*******","********","*********","**********"};

    /**
     * 生成n个星号的字符串
     * @param length
     * @return
     */
    private static String getStarChar(int length) {
        if (length <= 0) {
            return "";
        }
        //大部分敏感词汇在10个以内，直接返回缓存的字符串
        if (length <= 10) {
            return starArr[length - 1];
        }

        //生成n个星号的字符串
        char[] arr = new char[length];
        for (int i = 0; i < length; i++) {
            arr[i] = '*';
        }
        return new String(arr);
    }


    public static void main(String[] args) {
        WordTree tree = new WordTree();
        tree.addWord("朱家村");
        tree.addWord("香火");
        tree.addWord("土豆");
        tree.addWord("刚出锅");
        tree.addWord("吸毒");
        //正文
        String text = "像吸毒一样，爱上就无法放弃这些星座女";
        List<String> matchAll = tree.matchAll(text, -1, true, true);
        System.out.println(SensitiveWordUtils.replaceSensitiveWord(text,matchAll));

    }

}
